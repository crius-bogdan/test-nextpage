'use strict';
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    mainBowerFiles = require('main-bower-files'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    babel = require('gulp-babel'),
    browserSync = require("browser-sync"),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    pug = require('gulp-pug'),
    reload = browserSync.reload,
    svgSprite = require('gulp-svg-sprite'),
    svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace');

var path = {
    vendor: {
        js: 'app/js/vendor/',
        css: 'app/css/vendor/'
    },
    public: { 
        html: 'public/',
        js: 'public/js/',
        scss: 'public/css/',
        css: 'public/css/',
        img: 'public/img/',
        fonts: 'public/fonts/',
        template: 'public/template/'
    },
    app: { 
        html: 'app/*.html',
        js: 'app/js/*.js',
        scss: 'app/css/*.scss',
        css: 'app/css/*.css',
        scssIconFont: 'app/css/_icons.scss',
        img: 'app/img/**/*.*',
        icons: 'app/img/icons/*.svg',
        fonts: 'app/fonts/**/*.*',
        template: 'app/template/*.html',
        sprinteTemplate: 'app/css/template/_sprite_template.scss',
        sprinteFileScss: '.../../../_sprite-svg.scss',
        spinteAllSVG: 'app/img/sprinte/',
        pug: 'app/pug/*.pug',
        pugDop: 'app/pug/**/_*.pug'
    },
    watch: {
        html: 'app/**/*.html',
        js: 'app/js/**/*.js',
        scss: 'app/css/**/*.scss',
        css: 'app/css/**/*.css',
        img: 'app/img/**/*.*',
        fonts: 'app/fonts/**/*.*',
        template: 'app/template/*.html',
        pug: 'app/pug/*.pug',
        pugDop: 'app/pug/**/_*.pug'
    },
    clean: './public'
};
var config = {
    server: {
        baseDir: "./public"
    },
    tunnel: false,
    host: 'localhost',
    port: 3000,
    logPrefix: "Crius.Agency"
};

gulp.task('pug', function () {
    gulp.src([path.app.pug, '!' + path.app.pugDop])
        .pipe(plumber())
        .pipe(pug({
            pretty: true
        }))
        .on('error', notify.onError({
            message: "<%= error.message %>",
            title: "PUG Error!"
        }))
        .pipe(gulp.dest(path.public.html))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('svgSpriteBuild', function () {
    gulp.src(path.app.icons)
        // minify svg
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        // remove all fill, style and stroke declarations in out shapes
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: {
                xmlMode: true
            }
        }))
        // cheerio plugin create unnecessary string '&gt;', so replace it.
        .pipe(replace('&gt;', '>'))
        // build svg sprite
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: "../sprite.svg",
                    render: {
                        scss: {
                            dest: '../../../css/_sprite.scss',
                            template: path.app.sprinteTemplate
                        }
                    }
                }
            }
        }))
        .pipe(gulp.dest(path.app.spinteAllSVG));
});

gulp.task('vendorJs:build', function () {
    gulp.src( mainBowerFiles('**/*.js'))
        .pipe(gulp.dest(path.vendor.js))
        .pipe(gulp.dest(path.public.js));
});

gulp.task('vendorCss:build', function () {
    gulp.src( mainBowerFiles('**/*.css'))
        .pipe(gulp.dest(path.vendor.css))
        .pipe(gulp.dest(path.public.css));
});

// gulp.task('html:build', function () {
//     gulp.src(path.app.html)
//         .pipe(rigger())
//         .pipe(gulp.dest(path.public.html))
//         .pipe(reload({
//             stream: true
//         }));
// });

gulp.task('js:build', function () {
    gulp.src(path.app.js)
        // .pipe(rigger())
        // .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify().on('error', notify.onError({
            message: "<%= error.message %>",
            title: "JS Error 😡!"
        })))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.public.js))
        .pipe(reload({
            stream: true
        }));
        // .pipe(notify({
        //     message: "Все хорошо!",
        //     title: "JS Good 😁!"
        // }));
});

gulp.task('sass:build', function () {
    gulp.src(path.app.scss)
        // .pipe(sourcemaps.init())
        .pipe(sass().on('error', notify.onError({
                message: "<%= error.message %>",
            title: "Sass Error 😡!"
        })))
        .pipe(prefixer()) 
        .pipe(cssmin())
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.public.css))
        .pipe(reload({
            stream: true
        }));
        // .pipe(notify({
        //     message: "Все хорошо",
        //     title: "Sass Good 😁!"
        // }));
});

gulp.task('css:build', function () {
    gulp.src(path.app.css)
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.public.css))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(path.app.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.public.img))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('fonts:build', function() {
    gulp.src(path.app.fonts)
        .pipe(gulp.dest(path.public.fonts))
});

gulp.task('build', [
    'vendorCss:build',
    'vendorJs:build',
    // 'html:build',
    'js:build',
    'sass:build',
    'css:build',
    'fonts:build',
    'image:build',
    'pug'
]);

gulp.task('watch', function(){
    watch([path.watch.pug, path.watch.pugDop], function(event, cb) {
        gulp.start('pug');
    });
    // watch([path.watch.html], function(event, cb) {
    //     gulp.start('html:build');
    // });
    // watch([path.watch.template], function (event, cb) {
    //     gulp.start('html:build');
    // });
    watch([path.watch.scss], {readDelay: 500}, function(event, cb) {
        gulp.start('sass:build');
    });
    watch([path.watch.css], function(event, cb) {
        gulp.start('css:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});



// gulp.task('default', ['build', 'webserver', 'watch', 'move']);
gulp.task('default', ['build', 'webserver', 'watch']);
gulp.task('svgSprite', ['svgSpriteBuild']);