(function(doc){

    // import { Swiper, Navigation, Pagination, Scrollbar } from "swiper.esm.js";

    function getElem(name, wrap) {
        if (wrap) {
            return wrap.querySelector(name);
        } else {
            return doc.querySelector(name);
        }
    }
    function getElems(name, wrap) {
        if (wrap) {
            return wrap.querySelectorAll(name);
        } else {
            return doc.querySelectorAll(name);
        }
    }
    function atr(elem, attrubute) {
        return elem.getAttribute(attrubute);
    }
    function match(element, className) {
        return element[clas].contains(className);
    }
    const paralaxElems = getElems('.paral'),
          header = getElem('.header'),
          mouse = getElem('.mouse'),
          mouseText = getElem('.hov-text'),
          headerShadow = getElem('.shadow'),
          viewers = getElem('.of-v-num'),
          clientSect = getElem('.cs'),
          headerBg = getElem('.bg', header),
          clientBg = getElem('.bg', clientSect),
          imgClient1 = getElem('.header__bg-img_1'),
          imgClient2 = getElem('.header__bg-img_2'),
          weDoSect = getElem('.wd'),
          teamSect = getElem('.ts'),
          weDoBg = getElem('.wd .bg'),
          weDoItems = getElems('.w-items'),
          weDoBtns = getElems('.w-btn'),
          numFormatter = new Intl.NumberFormat("ru"),
          clas = 'classList',
          parentEl = 'parentNode',
          textEl = 'textContent',
          clientSectPosition = getCoords(clientSect),
          imgPosition = getCoords(imgClient1),
          teamSectPosition = getCoords(teamSect);
    // console.log(weDoSectPosition)
    let viewersNumber = parseInt(viewers[textEl].replace(/\D+/g, ''));
    console.log(clientSect.offsetHeight);
    clientSect.style.top = `-${clientSect.offsetHeight + 75}px`;
    function paralaxElem(e) {

        let halfWidth = header.offsetWidth / 2,
            halfHeight = header.offsetHeight / 2;
        
        if (match(e.target, 'offer__title')) {
            headerShadow[clas].add('big');
        } else if (match(e.target, 'm-hov')) {
            headerShadow[clas].add('medium');
        } else {
            headerShadow[clas].remove('big');
            headerShadow[clas].remove('medium');
        }

        headerShadow.style.transform = `translate3d(${e.clientX}px, ${e.clientY}px, 0)`;
        
        paralaxElems.forEach(el => {
            let Y = (e.clientY - halfHeight) / 10,
                X = (e.clientX - halfWidth) / 30;

            el.style.transform = `rotateY(${X}deg) rotateX(${Y}deg)`;
        });
    };
    function clearParalElem() {
        paralaxElems.forEach(el => {
            el.style.transform = `rotateY(0deg) rotateX(0deg)`;
        });
    };

    function reandomNumOnFirstScreen() {
        let randomNum = Math.floor(Math.random() * 100);

        viewersNumber += randomNum;
        viewers[textEl] = numFormatter.format(viewersNumber);
    } 

    setInterval(reandomNumOnFirstScreen, 1000);
    

    header.addEventListener('mousemove', paralaxElem);
    header.addEventListener('mouseout', clearParalElem);

    doc.addEventListener('mousemove', function(e) {
        let target = e.target,
            x = e.clientX,
            y = e.clientY;

        mouse.style.transform = `translate3d(${x}px, ${y}px, 0)`;
        // console.log(target);
        if (match(target, 'm-hov')) {
            mouse[clas].remove('hov-l');
            mouse[clas].add('hov-b');
            
            mouseText[textEl] = e.target.dataset.hover
            
        } else if (match(target, 'l-hov'))  {
            mouse[clas].remove('hov-b');
            mouse[clas].add('hov-l');
            mouseText[textEl] = '';

        } else {
            mouse[clas].remove('hov-b');
            mouse[clas].remove('hov-l');
            mouseText[textEl] = '';
        }
    });
    function getCoords(elem) {
        var box = elem.getBoundingClientRect();
        // console.log(box)

        return box.top + pageYOffset;

    }
    function headerBGColor(num) {
        headerBg.style.backgroundColor = `rgba(59, 28, 51, 0.${num})`;
    }
    function clientBGColor(num) {
        clientBg.style.backgroundColor = `rgba(59, 28, 51, 0.${num})`;
    }
    function weDoBGColor(num) {
        weDoBg.style.backgroundColor = `rgba(59, 28, 51, 0.${num})`;
    }

    function percent(a, b) {
        if(a == null) {
            return 0
        }
        var c = (a * 100) / b;
        return Math.floor(c);
    }

    doc.addEventListener('scroll', function(e){
        let percForHeroSect = percent(pageYOffset, clientSectPosition),
            percForSecondSect = percent((pageYOffset >= clientSectPosition) ? pageYOffset : null, getCoords(weDoSect)),
            percForThirdSect = percent((pageYOffset >= getCoords(weDoSect)) ? pageYOffset : null, getCoords(teamSect))

        // console.log(getCoords(weDoSect), 'func weDoSectPosition');
        // console.log(weDoSectPosition, 'var weDoSectPosition');
        // console.log(pageYOffset, 'pageYOffset');
        // console.log(getCoords(teamSect), 'func teamSectPosition');
        // console.log(teamSectPosition, 'var teamSectPosition');
        console.log(pageYOffset >= imgPosition);
        // console.log(percForSecondSect);
        if(pageYOffset >= imgPosition) {
            imgClient1.style.position = 'fixed';
        }
        // imgPosition
        if (!(percForHeroSect > 100) && !(percForHeroSect < 30)) {
            headerBGColor(percForHeroSect);
        } else if (percForHeroSect < 30) {
            headerBGColor(0);
        } else if (percForHeroSect > 100) {
            headerBGColor(9)
        }

        if (!(percForSecondSect > 100) && !(percForSecondSect < 70)) {
            clientBGColor(percForSecondSect);
        } else if (percForSecondSect < 70) {
            clientBGColor(0);
        } else if (percForSecondSect > 100) {
            clientBGColor(9)
        }

        if (!(percForThirdSect > 100) && !(percForThirdSect < 80)) {
            weDoBGColor(percForThirdSect);
        } else if (percForThirdSect < 80) {
            weDoBGColor(0);
        } else if (percForThirdSect >= 100) {
            weDoBGColor(9);
        } 
        // console.log(pageYOffset + ' pageYOffset', weDoSectPosition + ' weDoSectPosition');
    });
    doc.addEventListener('click', function(e){
        let target = e.target;

        while(target != this) {

            if(match(target, 'w-btn')) {
                let idxTarget = atr(target, 'data-id');
                
                weDoBtns.forEach(el => {
                    let idxEl = atr(el, 'data-id');

                    if (idxEl == idxTarget) {
                        el[clas].add('active');
                        // console.log(el);
                    } else {
                        el[clas].remove('active');
                    }
                    
                });
                weDoItems.forEach(el => {
                    let idxItem = atr(el, 'data-id');
                    
                    if (idxItem == idxTarget) {
                        el[clas].add('active');
                    } else {
                        el[clas].remove('active');
                    }
                });
            }

            target = target[parentEl];
        }
    });

    // Swiper.use([Navigation]);
    // console.log(Swiper);
    
    const teamSlider = new Swiper('.team-slider', {
        slidesPerView: 4,
        spaceBetween: 17,
        navigation: {
            nextEl: '.team__btn-next',
            prevEl: '.team__btn-prev',
        },
    });

})(document);